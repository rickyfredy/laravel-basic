<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

use DB;

use App\Models\User;

class SignUpController extends Controller
{
  public function signUpPage(Request $request)
  {
    return view('sign-up/sign-up-form');
  }

  public function doSignUp(Request $request)
  {
    $params = $request->only(['name','username','email','password','password_confirmation']);

    $message = [
      'name.required' => 'Name is required',
      'username.required' => 'Username is required',
      'email.required' => 'Email is required',
      'password.required' => 'Password is required',
      'password_confirmation.required' => 'Password Confirmation is required',
      'email.email' => 'Invalid email address',
      'password_confirmation.confirmed' => 'Password confirmation didn\'t match',
      'username.unique' => 'Username has been used by other user',
      'email.unique' => 'Email has been used by other user',
    ];

    $validator = Validator::make($params, [
      'name' => 'required',
      'username' => 'required|unique:user,username',
      'email' => 'required|email|unique:user,email',
      'password' => 'required|confirmed',
    ], $message);

    if ($validator->fails()) {
      return redirect('/sign-up')
              ->withError($validator->errors()->first());
    }

    DB::table('user')->insert([
      'name' => $params['name'],
      'username' => $params['username'],
      'email' => $params['email'],
      'password' => Hash::make($params['password']),
      'status' => '1',
      'created_at' =>  DB::RAW('NOW()'),
      'updated_at' =>  DB::RAW('NOW()'),
    ]);

    dd('goood');
  }
}

<?php

namespace App\Http\Controllers;

use DB;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;

use App\Constant\Message;
use App\Models\User;
use App\Models\ForgotPassword;

class ForgotController extends Controller
{
  const RESET_PASSWORD_TOKEN_LIFETIME_MINUTES = 60;

  public function forgotPasswordPage(Request $request)
  {
    return view('forgot/forgot-password-form');
  }

  public function doSendLink(Request $request)
  {
    $params = $request->only(['username']);

    $message = [
      'username.required' => 'Username or email is required',
    ];

    $validator = Validator::make($params, [
        'username' => 'required',
    ], $message);

    if ($validator->fails()) {
      return redirect('/forgot')
        ->withError(Message::FORGOT_PASSWORD_MSG_ERR);
    }

    $user = User::where(function($query) use ($request) {
        $query->where('username', $request->input('username'))
          ->orWhere('email', $request->input('username'));
      })
      ->where('status', 1)
      ->first();

    if ($user == null){
      return redirect('/forgot')
        ->withError(Message::FORGOT_PASSWORD_MSG_ERR);
    }

    DB::table('forgot_password')->insert([
      'user_id' => $user->id,
      'token' => Str::random(256),
      'status' => '1',
      'created_at' =>  DB::RAW('NOW()'),
      'updated_at' =>  DB::RAW('NOW()'),
    ]);


    // Todo: Send email to user


    return view('forgot/success-forgot');
  }

  public function resetPasswordPage(Request $request)
  {
    $params = $request->only(['token']);

    $message = [
      'token.required' => 'Reset Password Token is Required'
    ];

    $validator = Validator::make($params, [
        'token' => 'required',
    ], $message);

    if ($validator->fails()) {
      abort(400, $validator->errors()->first());
    }

    $forgot = ForgotPassword::whereRaw('token = ? AND status = 1 AND DATE_ADD(created_at, INTERVAL ? MINUTE) > NOW()',
                  [ $params['token'], self::RESET_PASSWORD_TOKEN_LIFETIME_MINUTES ])
                ->first();

    if (empty($forgot)) {
      abort(400, Message::RESET_TOKEN_EXPIRED_MSG_ERR);
    }

    $data['token'] = $params['token'];

    return view('forgot/reset-password-form', $data);
  }

  public function doResetPassword(Request $request)
  {
    $params = $request->only(['token','password','password_confirmation']);

    $message = [
      'token.required' => 'Reset password token is required',
      'password.required' => 'New password required',
      'password_confirmation.required' => 'New password confirmation required',
      'password_confirmation.confirmed' => 'Password confirmation didn\'t match',
    ];

    $validator = Validator::make($params, [
        'token' => 'required',
        'password' => 'required|confirmed',
        'password_confirmation' => 'required',
    ], $message);

    if ($validator->fails()) {
      return redirect('/reset?token=' . $params['token'])
        ->withError($validator->errors()->first());
    }

    $forgot = ForgotPassword::whereRaw('token = ? AND status = 1 AND DATE_ADD(created_at, INTERVAL ? MINUTE) > NOW()',
                  [ $params['token'], self::RESET_PASSWORD_TOKEN_LIFETIME_MINUTES ])
                ->first();


    if (empty($forgot)) {
      abort(400, Message::RESET_TOKEN_EXPIRED_MSG_ERR);
    }

    $forgotUser = User::find($forgot->user_id);

    if (empty($forgotUser)) {
      abort(400, Message::RESET_TOKEN_EXPIRED_MSG_ERR);
    }

    $forgotUser->password = Hash::make($params['password']);
    $forgotUser->save();

    $forgot->status = 0;
    $forgot->save();

    return view('forgot/success-reset');
  }
}
<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

use App\Constant\Message;

class SignInController extends Controller
{
  public function signinPage(Request $request)
  {
    return view('sign-in/sign-in-form');
  }

  public function doSignIn(Request $request)
  {
    $params = $request->only(['username','password']);

    $message = [
      'username.required' => 'Username or email is required',
      'password.required' => 'Password or email is required',
    ];

    $validator = Validator::make($params, [
        'username' => 'required',
        'password' => 'required',
    ], $message);

    if ($validator->fails()) {
      return redirect('/sign-in')->withError($validator->errors()->first());
    }

    if (Auth::attempt([
      'username' => $request->input('username'),
      'password' => $request->input('password'),
      'status' => '1',
    ], $request->input('remember', false))){
      $request->session()->regenerate();

      return redirect('/blank');
    }


    return redirect('/sign-in')->withError(Message::AUTH_MSG_ERR);
  }

  public function doSignOut(Request $request)
  {
    Auth::logout();

    $request->session()->invalidate();

    $request->session()->regenerateToken();

    return redirect('/sign-in');
  }
}
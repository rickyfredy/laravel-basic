<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BlankController extends Controller
{
    public function blank(Request $request)
    {
        return view('blank/blank');
    }
    
}
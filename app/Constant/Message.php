<?php

namespace App\Constant;

class Message
{
	public const AUTH_MSG_ERR = 'Invalid username or password';

	public const FORGOT_PASSWORD_MSG_ERR = 'Username or email not registered';

	public const RESET_TOKEN_EXPIRED_MSG_ERR = 'Reset password token expired';
}
@extends('template.error-master')

@section('title', 'Success Send Verification')

@section('style')
@endsection

@section('content')
  <!-- Page -->
  <div class="page vertical-align text-center" data-animsition-in="fade-in" data-animsition-out="fade-out">
    <div class="page-content vertical-align-middle">
      <header>
        <p>Success Verify Your Username</p>
      </header>
      <p class="error-advise">Reset Password Link Has Send to Your Email, Please Check Your Inbox</p>
      <a class="btn btn-primary btn-round" href="/sign-in">GO TO SIGN IN PAGE</a>

      <footer class="page-copyright">
        <p>WEBSITE BY {{ env('APP_OWNER_NAME') }}</p>
        <p>© {{ env('APP_PUBLISH_YEAR') }}. All RIGHT RESERVED.</p>
      </footer>
    </div>
  </div>
  <!-- End Page -->
@endsection

@section('script')
@endsection
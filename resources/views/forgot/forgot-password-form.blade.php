@extends('template.forgot-password-master')

@section('title', 'Forgot Password')

@section('style')
  <!-- alertify -->
  <link rel="stylesheet" href="/global/vendor/alertify/alertify.css">
  <link rel="stylesheet" href="/global/vendor/notie/notie.css">

  <!-- Page -->
  <link rel="stylesheet" href="/assets/examples/css/pages/forgot-password.css">
@endsection

@section('content')
  <!-- Page -->
  <div class="page vertical-align text-center" data-animsition-in="fade-in" data-animsition-out="fade-out">
    <div class="page-content vertical-align-middle">
      <h2>Forgot Your Password ?</h2>
      <p>Input your registered email or username to reset your password</p>

      <form method="post" role="form" autocomplete="off" action="/forgot">
        @csrf
        <div class="form-group form-material floating" data-plugin="formMaterial">
          <input type="text" class="form-control empty" id="inputUsername" name="username">
          <label class="floating-label" for="inputUsername">Your Username or Email</label>
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-primary btn-block">Verify Username or Email</button>
        </div>
      </form>
      <p><a href="/sign-in">Sign In</a></p>

      <footer class="page-copyright">
        <p>WEBSITE BY {{ env('APP_OWNER_NAME') }}</p>
        <p>© {{ env('APP_PUBLISH_YEAR') }}. All RIGHT RESERVED.</p>
      </footer>
    </div>
  </div>
  <!-- End Page -->
@endsection

@section('script')
  <!-- alertify -->
  <script src="/global/vendor/alertify/alertify.js"></script>
  <script src="/global/vendor/notie/notie.js"></script>

  <!-- material -->
  <script src="/global/js/Plugin/material.js"></script>

  <script type="text/javascript">
    
    $(document).ready(function(){
    
      @if (session()->get('error'))
        alertify.alert("{{ session()->get('error') }}");
      @endif

    });

  </script>
@endsection
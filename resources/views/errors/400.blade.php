@extends('template.error-master')

@section('title', 'Error 400')

@section('style')
@endsection

@section('content')
  <!-- Page -->
  <div class="page vertical-align text-center" data-animsition-in="fade-in" data-animsition-out="fade-out">
    <div class="page-content vertical-align-middle">
      <header>
        <h1 class="animation-slide-top">400</h1>
        <p>Please Check Your Data</p>
      </header>
      <p class="error-advise">{{ $exception->getMessage() }}</p>
      <a class="btn btn-primary btn-round" href="/sign-in">GO TO HOME PAGE</a>

      <footer class="page-copyright">
        <p>WEBSITE BY {{ env('APP_OWNER_NAME') }}</p>
        <p>© {{ env('APP_PUBLISH_YEAR') }}. All RIGHT RESERVED.</p>
      </footer>
    </div>
  </div>
  <!-- End Page -->
@endsection

@section('script')
@endsection
@extends('template.sign-in-master')

@section('title', 'Sign In | Authentication')

@section('style')
  <!-- alertify -->
  <link rel="stylesheet" href="/global/vendor/alertify/alertify.css">
  <link rel="stylesheet" href="/global/vendor/notie/notie.css">

  <!-- Page -->
  <link rel="stylesheet" href="/assets/examples/css/pages/login-v3.css">
  <link rel="stylesheet" href="/assets/examples/css/advanced/alertify.css"> 
@endsection

@section('content')
  <!-- Page -->
  <div class="page vertical-align text-center" data-animsition-in="fade-in" data-animsition-out="fade-out">>
    <div class="page-content vertical-align-middle">
      <div class="panel">
        <div class="panel-body">
          <div class="brand">
            <!-- <img class="brand-img" src="/assets//images/logo-colored.png" alt="..."> -->
            <h2 class="brand-text font-size-18">Warehouse Management System</h2>
          </div>

          <form method="post" action="/sign-in" autocomplete="off">
            @csrf
            <div class="form-group form-material floating" data-plugin="formMaterial">
              <input type="text" class="form-control" name="username" />
              <label class="floating-label">Username</label>
            </div>
            <div class="form-group form-material floating" data-plugin="formMaterial">
              <input type="password" class="form-control" name="password" />
              <label class="floating-label">Password</label>
            </div>
            <div class="form-group clearfix">
              <div class="checkbox-custom checkbox-inline checkbox-primary checkbox-lg float-left">
                <input type="checkbox" id="inputCheckbox" name="remember">
                <label for="inputCheckbox">Remember me</label>
              </div>
              <a class="float-right" href="/forgot">Forgot password?</a>
            </div>

            @if (session()->get('error'))
              <div class="alert alert-danger alert-dismissible" role="alert">
                {{ session()->get('error') }}
              </div>
            @endif

            <button type="submit" class="btn btn-primary btn-block btn-lg mt-40">Sign in</button>
          </form>
          <p>Still no account? Please go to <a href="/sign-up">Sign up</a></p>
        </div>
      </div>

      <footer class="page-copyright page-copyright-inverse">
        <p>WEBSITE BY {{ env('APP_OWNER_NAME') }}</p>
        <p>© {{ env('APP_PUBLISH_YEAR') }}. All RIGHT RESERVED.</p>
      </footer>
    </div>
  </div>
  <!-- End Page -->
@endsection

@section('script')
  <!-- alertify -->
  <script src="/global/vendor/alertify/alertify.js"></script>
  <script src="/global/vendor/notie/notie.js"></script>

  <!-- placehodler -->
  <script src="/global/vendor/jquery-placeholder/jquery.placeholder.js"></script>
  <script src="/global/js/Plugin/jquery-placeholder.js"></script>
  <script src="/global/js/Plugin/material.js"></script>
  <script src="/global/js/Plugin/alertify.js"></script>
  <script src="/global/js/Plugin/notie-js.js"></script>
@endsection
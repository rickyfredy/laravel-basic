@extends('template.sign-up-master')

@section('title', 'Sign Up | Registration')

@section('style')
  <!-- Page -->
  <link rel="stylesheet" href="/assets/examples/css/pages/register-v3.css">
@endsection

@section('content')
  <!-- Page -->
  <div class="page vertical-align text-center" data-animsition-in="fade-in" data-animsition-out="fade-out">>
    <div class="page-content vertical-align-middle">
      <div class="panel">
        <div class="panel-body">
          <div class="brand">
            <h2 class="brand-text font-size-18">Warehouse Management System</h2>
          </div>
          <form method="post" action="/sign-up" autocomplete="off">
            @csrf
            <div class="form-group form-material floating" data-plugin="formMaterial">
              <input type="text" class="form-control" name="name" />
              <label class="floating-label">Full Name</label>
            </div>
            <div class="form-group form-material floating" data-plugin="formMaterial">
              <input type="text" class="form-control" name="username" />
              <label class="floating-label">Username</label>
            </div>
            <div class="form-group form-material floating" data-plugin="formMaterial">
              <input type="email" class="form-control" name="email" />
              <label class="floating-label">Email</label>
            </div>
            <div class="form-group form-material floating" data-plugin="formMaterial">
              <input type="password" class="form-control" name="password" />
              <label class="floating-label">Password</label>
            </div>
            <div class="form-group form-material floating" data-plugin="formMaterial">
              <input type="password" class="form-control" name="password_confirmation" />
              <label class="floating-label">Re-enter Password</label>
            </div>

            @if (session()->get('error'))
              <div class="alert alert-danger alert-dismissible" role="alert">
                {{ session()->get('error') }}
              </div>
            @endif

            <button type="submit" class="btn btn-primary btn-block btn-lg mt-40">Sign up</button>
          </form>
          <p>Have account already? Please go to <a href="/sign-in">Sign In</a></p>
        </div>
      </div>

      <footer class="page-copyright page-copyright-inverse">
        <p>WEBSITE BY {{ env('APP_OWNER_NAME') }}</p>
        <p>© {{ env('APP_PUBLISH_YEAR') }}. All RIGHT RESERVED.</p>
      </footer>
    </div>
  </div>
  <!-- End Page -->
@endsection

@section('script')
  <!-- placehodler -->
  <script src="/global/vendor/jquery-placeholder/jquery.placeholder.js"></script>
  <script src="/global/js/Plugin/jquery-placeholder.js"></script>
  <script src="/global/js/Plugin/material.js"></script>
@endsection
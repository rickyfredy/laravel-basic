<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SignInController;
use App\Http\Controllers\BlankController;
use App\Http\Controllers\ForgotController;
use App\Http\Controllers\SignUpController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// ['first', 'second'])->group(function () {
Route::middleware([])->group(function(){

	Route::get('/sign-in', [SignInController::class, 'signinPage']);
	Route::post('/sign-in', [SignInController::class, 'doSignIn']);

	Route::get('/sign-out', [SignInController::class, 'doSignOut']);


	Route::get('/forgot', [ForgotController::class, 'forgotPasswordPage']);
	Route::post('/forgot', [ForgotController::class, 'doSendLink']);
	Route::get('/reset', [ForgotController::class, 'resetPasswordPage']);
	Route::post('/reset', [ForgotController::class, 'doResetPassword']);

	Route::get('/sign-up', [SignUpController::class, 'signUpPage']);
	Route::post('/sign-up', [SignUpController::class, 'doSignUp']);


	Route::get('/blank', [BlankController::class, 'blank']);
});